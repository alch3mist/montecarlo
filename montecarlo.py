#!/usr/bin/env python3

##########################################
#                                        #
#           MONTE CARLO METHODS          #
#                                        #
##########################################
#                                        #
#               Description              #
#                                        #
#   This Library provides classical and  #
#   also advanced Monte Carlo Methods.   #
#           build with <3                #
##########################################


# ~ libraries
import numpy as np
from numpy import sin, cos, pi, sqrt, exp, log, log2, transpose, add, array, dot, insert, where
from numpy.random import uniform, randint, normal
from numpy.linalg import cholesky
from os import get_terminal_size, system
from time import sleep
import matplotlib.pyplot as plt
import threading


# --- generators and sampling --- #

# uniform
def u(*args, **kwargs):
    keys = []
    for key, value in kwargs.items():
        keys.append(key)
    if len(keys) + len(args) == 0:
        return uniform(0, 1)
    if len(args) == 0:
        if "low" in keys and "high" in keys:
            if "n" in keys:
                return uniform(kwargs["low"], kwargs["high"], kwargs["n"])
            else:
                return uniform(kwargs["low"], kwargs["high"])   
        elif ("low" in keys and "high" not in keys) or ("low" in keys and "high" not in keys):
            print("ERROR: low and high args require simultaneous persistence!")
    elif len(args) == 1:
        if type(args[0]) is list:
            if "n" in keys:
                return [args[0][i] for i in randint(0, high=len(args[0]), size=kwargs["n"])]
            else:
                return args[0][randint(0, high=len(args[0]))]
        elif args[0] is "d":
            if "low" in keys and "high" in keys:
                if "n" in keys:
                    return randint(kwargs["low"], high=kwargs["high"]+1, size = kwargs["n"])
                else:
                    return randint(kwargs["low"], high=kwargs["high"]+1)
            else:
                raise ValueError("ERROR: u(low=int(), high=int(),...) - low and high flags were not specified")    
        else:
            raise TypeError("ERROR: argument " + str(args[0]) + " is " + str(type(args[0])) + " must be list or 'd' - for discrete sampling..." )
    else:
        raise ValueError("ERROR: u(*args) takes only ONE argument but " + str(len(args)) + " were given... \n pass a list with numbers or 'd' for discrete samling.")

def throw(*args):
    out = []
    for i in args:
        if type(i) is list and len(i) == 2:
            out.append( u(low=i[0], high=i[1]) )
        elif len(i) > 2:
            out.append(u(i))
        else:
            raise TypeError("ERROR: throw([low, high],...): Arguments must be lists with length 2. \nFirst list element is lower rank and upper is second.")
    return out

class generator:

    def __init__(self, seed=None, b=False):
        if seed is None:
            self.seed = self.seedGen()
        else:
            self.seed = seed
        self.number = 0
        self.b = b

    def startStream(self):
        
        def iterate():
            while self.stream:
                self.neumann()
        
        self.stream = True
        thread = threading.Thread(target=iterate)
        thread.start()
    
    def stopStream(self):
        self.stream = False

    def seedGen(self, n=10):
        s = ""
        for i in range(n):
            s += str(u("d", low=0, high=9))
        self.seed = int(s)

    def neumann(self):
        l = len(str(self.seed))
        half = int(l/2)
        quart = int(half)
        
        # neumann step
        mid = int(str(self.seed)[half-quart:half+quart])
        newSeed = mid ** 2

        # overwrite
        if newSeed > l:
            newSeed = int(str(newSeed)[:l])
        self.seed = newSeed
        num = mid%10
        if self.b:
            self.number = num%2
        else:
            self.number = num

    def log(self):
        try:
            while True:
                sleep(0.02)
                print(self.number)
        except KeyboardInterrupt:
            pass



# non-uniform

def norm(mean, var, n=1):
    if var < 0:
        raise ValueError("ERROR: in normal(): parameter var is the variance and must be greater than 0, but v=" + str(var))
    return normal(loc=mean, scale=sqrt(var), size=n)

def boxMuller(n=1):
    if n > 1:
        arr = []
        for i in range(n):
            # generate uniformly distributed random variables
            u1, u2 = u(), u()

            # determine independent variables box-muller-transformed
            z = [ sqrt(-2*log(u1))*cos(2*pi*u2), sqrt(-2*log(u1))*sin(2*pi*u2)]

            arr.append(z)
        return arr
    else:
        # generate uniformly distributed random variables
        u1, u2 = u(), u()

        # determine independent variables box-muller-transformed
        z = [ sqrt(-2*log(u1))*cos(2*pi*u2), sqrt(-2*log(u1))*sin(2*pi*u2)]
        return z

def mGauss(mean, sigma, n=1):
    
    # define dimension
    dim = len(mean)

    # check if dimensions match
    if len(mean) != len(sigma):
        print("ERROR: Dimensions of mean and sigma do not match!")
        quit()
    
    # generate uniformed numbers
    q = norm(0, 1, dim)

    # try cholesky factorization
    try:
        L = cholesky(sigma)
        #print("L:",L)
    except:
        raise ValueError("ERROR: mGauss(..,sigma,..): From Cholesky: Covariance matrix sigma is not decomposable!")
        exit()
    
    arr = []
    if n > 1:
        for i in range(n):     
            # generate uniformed numbers
            q = norm(0, 1, dim)

            # get random gauss distributed vector
            y = L.dot(q)

            # finally shift random vector
            x = array(add(y, mean))

            arr.append(x)
        return arr
    else:
        # get random gauss distributed vector
        y = L.dot(q)

        # finally shift random vector
        x = array(add(y, mean))
        return x

def reject(f, g=None, G_inv=None, minRange=0, maxRange=1, n=1):

    if G_inv == None:
        # g is max of function
        _max_ = analysis.crudeFind("max", f, minRange = minRange, maxRange=maxRange)[1] + minRange
        
    
    # parse in functions
    def f_f(x):
        return eval(f)
    
    def F_g(x):
        if G_inv == None:
            # G_inv is max of function
            return x
        else:
            return eval(G_inv)
    
    def f_g(x):
        if g == None:
            return _max_
        else:
            return eval(g)
    

    # generate r. V. x distributed with g
    load, trys = [], 0
    for i in range(n):
       
        
        # generate r.V. with rejection and f distribution
        while True:
            try:
                trys += 1
                # generate r. V. y distributed with h
                # F_h^-1 for inversion sampling
                y = F_g( u(low=minRange, high=maxRange) )
                print(y)
                
                
                if u() <= f_f(y)/f_g(y):
                    load.append(y)
                    break
            except:
                pass
            
    
    # decide wether to return array or float
    if n == 1:
        return load[0]
    else:
        return load


# experiments
class experiment:

    def dice():
        return u("d", low=1, high=6)

    def collatz(min=1, logging=True, p=True): # with n whole number
        
        # log print
        if p:
            def cprint(text, col):
                if text == None:
                    text = "00"
                else:
                    text = str(text)
                if len(text) == 1:
                    text = " " + text
                if col == "green":
                    return '\033[0;37;42m' + text + '\033[0m'
                if col == "red":
                    return '\033[0;37;41m' + text + '\033[0m'
                if col == "black":
                    return '\033[0;37;48m' + text + '\033[0m'

        def cond(n):
            if int(n)%16 == 0:
                return True
            else:
                return False

        try:

            # integerize the starting number
            n = int(min)
            print("first", n)
            # init log
            if logging:
                f = open("./collatz_2^" + str(int(log2(n))) + ".dat", "w+")
                f.write("n          sequence-length\n-----------------------\n")
            
            while True:

                # generate number
                n = int(n + 1)
                
                
                # print current number
                if p:
                    print(n, cprint("trying...", "green"), 0, end="\r")

                # initialize the sequence length
                count = 0

                while cond(n) is not True:
                    if n%2 == 0:
                        n, count = int(n/2), count + 1
                    else:
                        n, count = int(3*n+1)/2, count + 2 
                    if p:   
                        print(n, cprint("trying...", "green"), count, end="\r")

                # print fail
                if p:
                    print(n, cprint("failed...", "red"))

                # log
                if logging:
                    f.write(str(n) +"\t"+ str(count) + "\n")

                #exit()
            
                    
        except KeyboardInterrupt:
            f.close()
           
    def buffon(throws, needleLength=1, gridDistance=2):     # needle throw with rejection method; returns approximated probability that the needle crosses a line 
        crossings = 0
        for i in range(int(throws)):
            t = throw( [0.0, gridDistance/2], [0.0, pi/2.] )
            if t[0] <= float((needleLength/2) * sin(t[1])):
                crossings += 1
        return float(throws)/crossings

    def shuffle(array):
        out =[]
        x = array.copy()
        for i in range(len(array)):
            pick = u(x)
            out.append(pick)
            x.remove(pick)
        return out

    def roulette():

        def cprint(text, col):
            if text == None:
                text = "00"
            else:
                text = str(text)
            if len(text) == 1:
                text = " " + text
            if col == "green":
                return '\033[0;37;42m' + text + '\033[0m'
            if col == "red":
                return '\033[0;37;41m' + text + '\033[0m'
            if col == "black":
                return '\033[0;37;48m' + text + '\033[0m'

        def table(position):
            class num(object):
            
                def __init__(self, colour, digit, position):
                    """ Create a new point at the origin """
                    self.colour = colour
                    self.digit = digit
                    self.position = position

            position = position % 37

            width = get_terminal_size().columns
            if width > 38:
                width = 38
            arr = [
                num("green", 0, 0),
                num("black", 28, 1),
                num("red", 9, 2),
                num("black", 26, 3),
                num("red", 30, 4),
                num("black", 11, 5),
                num("red", 7, 6),
                num("black", 20, 7),
                num("red", 32, 8),
                num("black", 17, 9),
                num("red", 5, 10),
                num("black", 22, 11),
                num("red", 34, 12),
                num("black", 15, 13),
                num("red", 3, 14),
                num("black", 24, 15),
                num("red", 36, 16),
                num("black", 13, 17),
                num("red", 1, 18),
                num("green", None, 19),
                num("red", 27, 20),
                num("black", 10, 21),
                num("red", 25, 22),
                num("black", 29, 23),
                num("red", 12, 24),
                num("black", 8, 25),
                num("red", 19, 26),
                num("black", 31, 27),
                num("red", 18, 28),
                num("black", 6, 29),
                num("red", 21, 30),
                num("black", 33, 31),
                num("red", 16, 32),
                num("black", 4, 33),
                num("red", 23, 34),
                num("black", 35, 35),
                num("red", 14, 36),
                num("black", 2, 37),
            ]
            if width % 2 == 0:
                a, b = int(position-width/2), int(position+width/2)
            else:
                a, b = int(position-width/2), int(position+width/2)
            if b > 37:
                b = b%37
                a = a - 37

            string = ""
            for i in range(a, b):
                #print(a,b)
                string += cprint(arr[i].digit, arr[i].colour)
            return string

        def roll():
            # entry
            entryPoint = u("d", low=0, high=37)
            tipOff = u("d", low=0, high=64)
            slowdown = u("d", low = 0, high = 4)
            # roll two rounds
            for i in range(63):
                print("                                 " + "°" + "                                ", end = "\r\n")
                print(table(i), end="\r")
                sleep(0.2)
                system("clear")
            
            for i in range(tipOff):
                print("                                 " + "°" + "                                ", end = "\r\n")
                print(table(i), end="\r")
                if tipOff - i < slowdown:
                    sleep(1.0)
                else:
                    sleep(0.5)
                if i < tipOff-1:
                    system("clear")
            print("\n")

        while True:
            roll()
            if input("Retry? (y/n)") == "y":
                system("clear")
            else:
                break

    def montyHall(*args, tries=100):
        doors_template, numbers = ["Goat", "Car", "Goat"], ["1","2","3"]
        def c(doors, door=None, number=None):
            if door != None:
                return str(doors.index(door)+1)
            else:
                return doors[number-1]

        success = 0
        for i in range(tries):

            # shuffle the doors
            doors = experiment.shuffle(doors_template)
            
            # pick one door
            if "auto" in args:
                pick = u(numbers)
            else:
                pick = input("pick a door 1,2,3:")
           
            # give the moderator information
            num = numbers.copy()
            index_car = c(doors, door="Car")

            # moderator opens one goat but not the pick
            if pick != index_car:
                num.remove(index_car)
                num.remove(pick)
                Open = num[0]
            else:
                num.remove(index_car)
                Open = u(num)
            if "auto" not in args:
                print("moderator opened door number " + str(Open) + " with a goat behind...")
                sleep(1)

            # want to switch
            num = numbers.copy()
            num.remove(pick)
            num.remove(Open)
            if "auto" not in args:
                if input("do you want to switch to door number "+num[0]+"? (y/n)") == "y":
                    pick = num[0]
            else:
                pick = num[0]

            # open pick
            if "auto" not in args:
                print("moderator opens your pick (door " + pick + ") \n where behind is a " + doors[int(pick)-1])
                sleep(1)

            # count if car
            if doors[int(pick)-1] == "Car":
                success += 1
            
        print("Success-rate:", success/tries)



# --- integration --- #
class integration:

    def crude(fct, minRange=0, maxRange=1, N=100, setSize=1): # function as string with x as variable
        # build function
        f = lambda x: eval(fct)
        #def f(x):
        #    return exp(-x**2)
        
        out = []
        for i in range(setSize):
            out.append((maxRange - minRange) * sum([f(x) for x in u(low=minRange, high=maxRange, n=N)])/N)

        # return integral
        if setSize == 1:
            return out[0]
        else:
            return out

    def stratified(fct, minRange=0, maxRange=1, bins=10, N_per_bin=10, setSize=1):
        
        def f(x):
            return eval(fct)

        out = []
        for i in range(setSize):
        
            sum = 0
            delta = (maxRange - minRange) / bins
            for i in range(bins):
                
                interval = [float(i * delta + minRange), float((i+1) * delta + minRange)]
                sample = u(low=interval[0], high=interval[1], n=N_per_bin)
                vol = float(interval[1] - interval[0])
                subInt = 0
                for j in sample:
                    subInt += f(j)
                sum += (vol/N_per_bin)*subInt
            out.append(sum)
        
        if setSize == 1:
            return out[0]
        else:
            return out

    def antithetic(fct, minRange=0, maxRange=1, N=100, setSize = 1):   # variance reducing
        f_hat = "0.5*(" + fct + " + " + fct.replace("x","(1-x)") + ")"
        return integration.crude(f_hat, minRange=minRange, maxRange=maxRange, N=N, setSize=setSize)



# --- importance sampling --- #
class markov:

    def chain(H, alg="metropolis", start=None, space=None, stdout=False, conservative=False, steps=None, sampling=1): # alg can be metropolis or glauber

        def f(x):
            return H(x)

        # init output
        out = [start]

        # validate dimension
        if start[0] is list:
            if start[0][0] is list:
                dim = 3
            else:
                dim = 2
        else:
            dim = 1

        # dimension lenth
        l = len(start)

        # metropolis hastings
        if "metropolis" in alg:

            # create new candidate
            if conservative:
                
                # init first state
                x = start

                # simulate
                for t in range(steps):

                    # pick randomly proper annihilation and creation site
                    while True:
                        s_a = u("d", low=0, high=l-1, n=dim)
                        s_c = u("d", low=0, high=l-1, n=dim)

                        if dim == 1:
                            x_a, x_c = x[s_a[0]], x[s_c[0]]
                        elif dim == 2:
                            x_a, x_c = x[s_a[0]][s_a[1]], x[s_c[0]][s_c[1]]
                        elif dim == 3:
                            x_a, x_c = x[s_a[0]][s_a[1]][s_a[2]], x[s_c[0]][s_c[1]][s_c[2]]

                        if s_a != s_c and x_a != 0:
                            break
                    
                    # create candidate
                    x_prime = x.copy()
                    if dim == 1:
                        x_prime[s_a[0]] -= 1
                        x_prime[s_c[0]] += 1
                    elif dim == 2:
                        x_prime[s_a[0]][s_a[1]] -= 1
                        x_prime[s_c[0]][s_c[1]] += 1
                    elif dim == 3:
                        x_prime[s_a[0]][s_a[1]][s_a[2]] -= 1
                        x_prime[s_c[0]][s_c[1]][s_c[2]] += 1
    

                    # selection
                    a, b = f(x_prime), f(x)
                    if u() < min(1, a/b):
                        x = x_prime.copy()

                    if t % sampling == 0:
                        out.append(x)

                return out   
                         
    def ising(**kwargs):	# (algorithm, steps, space, store=array(), length, P, [ order ] )

        # create store
        out = []
        
        # draw keys
        keys, values = [], []
        for key, value in kwargs.items():
            keys.append(key)
            values.append(value)
        if "stdout" in keys:
            if kwargs["stdout"]:
                print("~ TEST SUITE ~")
                for k in range(len(keys)):
                    print(str(keys[k]), ":", str(values[k]))

        # draw mandatory args
        try:
            n = kwargs["steps"]
        except:
            n = 1
        try:
            S = kwargs["space"]
        except:
            raise ValueError("ERROR: state space not declared.")
        try:
            N = kwargs["length"]
        except:
            N = 10
        try:
            j_v, j_h = kwargs["j_v"], kwargs["j_h"]
        except:
            j_v, j_h = 1., 1.

        
        # draw optional
        try:
            order = kwargs["order"]
        except:
            order = 1
        try: 
            sampling = kwargs["sampling"]
        except:
            sampling = 1
        try:
            file = open(kwargs["log"], "w+")
            file.close()
        except:
            pass

        # --- ALGORITHM --- #
        if True:   
            # parse Temperature
            try:
                T = kwargs["T"]
                #beta = 1/( 1.38064852e-23 * T )
                beta = 1 / T
            except:
                raise ValueError("ERROR: temperature not declared.")
               
            
            # generate starting state and save it
            try:
                if kwargs["start"] == "cold":
                    print("cold")
                    x0 = [[max(S)]*N for i in range(N)]
                elif kwargs["start"] == "hot":
                    x0 = [u(S, n=N) for i in range(N)]
                elif type(kwargs["start"]) == list:
                    x0 = kwargs["start"]
            except:
                #raise Warning("WARNING: start mode hot/cold not defined. 'hot' was chosen!")
                x0 = [u(S, n=N) for i in range(N)]
            out.append(array(x0))

            # random walk
            count, x, _range = 0, array(x0), [var for var in range(N)]
            while count < n:
                
                k, l, y = u(_range), u(_range), x.copy()
                S.remove(x[k][l])
                y[k][l] = u(S)
                S.append(x[k][l])

                #2nd: validate energy difference
                ######### ISING IMPROVED HAMILTON D IFFERENCE ##########
                
                #print("x", x[k][l], "y", y[k][l])
                # boundaries
                if l == 0:
                    l_prior = N-1
                else:
                    l_prior = l-1
                if l == N-1:
                    l_post = 0
                else:
                    l_post = l+1
                if k == 0:
                    k_prior = N-1
                else:
                    k_prior = k-1
                if k == N-1:
                    k_post = 0
                else:
                    k_post = k+1
                
                # proved energy difference, simplified
                dH = 2 * x[k][l] * ( j_v * ( x[k][l_post] + x[k][l_prior] ) + j_h * ( x[k_prior][l] + x[k_post][l] ) )
                
                ########################################################
                # define alpha
                alpha = exp( -beta * dH ) # independent of Z

                
                # 3rd: accept or reject
                if u() <= alpha:
                    x = y
                    count += 1
                    # store data
                    if (count % sampling) == 0:
                        out.append(array(x))
                        if "log" in keys:
                            file = open(kwargs["log"], "a")
                            file.write(str(x))
                            file.close()
                            
                    if "stdout" in keys:
                        if kwargs["stdout"]:
                            # -- print -- #
                            guiWidth = get_terminal_size().columns
                            loadString = "".join(["|"] * int( (count / n) * guiWidth ) )
                            print(loadString, end='\r')
                else:
                    pass
        return out



# --- data analysis --- #
class analysis:

    def chi_squared(data_array, model_array, mode="rel", order=2):
        square = [] 
        if len(model_array) != len(data_array):
            raise ValueError("ERROR: model-array must have same length as data_array!")
        
        for i in range(len(data_array)):
            if mode == "rel":
                square.append( ((data_array[i]-model_array[i])**order)/model_array[i] )
            elif mode == "abs":
                square.append( ((data_array[i]-model_array[i])**order) )
        return sum(square)
    
    def indep(data1, data2, bins=None):

        # provide list form
        data1, data2 = [data1[j] for j in range(len(data1))], [data2[k] for k in range(len(data2))]

        # define space
        if data1[0] is not float and data2[0] is not float:
            space1 = [data1[0]]
            for d in data1:
                if d not in space1:
                    space1.append(d)
            space2 = [data2[0]]
            for d in data2:
                if d not in space2:
                    space2.append(d)
        else:
            # find extremas
            # data 1
            if data1[0] is float:
                min1, max1 = min(data1), max(data1)
                delta1 = (max1-min1)/bins
                space1 = [ [i*delta1, (i+1)*delta1] for i in range(bins-1) ]
            # data 2
            if data2[0] is float:
                min2, max2 = min(data2), max(data2)
                delta2 = (max2-min2)/bins
                space2 = [ [i*delta2, (i+1)*delta2] for i in range(bins-1) ]

        # try to sort space if number input
        try:
            space1.sort()
            space2.sort()
        except:
            pass

        

        # calc distributions
        dist1, dist2 = [], []
        if type(data1[0]) is not float and type(data2[0]) is not float:
            if space1 == space2:
                for s in space1:
                    dist1.append( data1.count(s) )
                    dist2.append( data2.count(s) )
            else:
                for s in space1:
                    dist1.append( data1.count(s) )
                for s in space2:
                    dist2.append( data2.count(s) )
        else:
            if data1[0] is float:
                dist1 = [0]*bins
                for bin in range(bins):
                    for j in data1:
                        if space1[bin][0] <= j  and j >= space1[bin][0]:
                            dist1[bins] += 1
            if data2[0] is float:
                dist2 = [0]*bins
                for bin in range(bins):
                    for j in data2:
                        if space2[bin][0] <= j  and j >= space2[bin][0]:
                            dist2[bins] += 1
        m, r = len(dist1), len(dist2)

        # integrate n
        N = 0
        for j in range(len(dist1)):
            for k in range(len(dist2)):
                N += dist1[j]*dist2[k]
        
        def n(j, k):
            return dist1[j]*dist2[k]/N

        # build convolved relative place occurences
        n_j_dot = [sum( [n(j,k) for k in range(r)] ) for j in range(m)]
        n_dot_k = [sum( [n(j,k) for j in range(m)] ) for k in range(r)]
        
        def n_star(j, k):
            return n_j_dot[j]*n_dot_k[k]/N

        # calculate chi squared estimate
        out = 0
        for j in range(len(dist1)):
            for k in range(len(dist2)):
                out += 1/n_star(j, k)**2*(n(j, k) - n_star(j, k))**2

        return out

    def mean(dataSet):

        if type(dataSet[0]) is not list:
            return sum(dataSet)/len(dataSet)
        else:
            out = []
            for i in range(len(dataSet[0])):
                s = 0
                for j in range(len(dataSet)):
                    s += dataSet[j][i]
                out.append(float(s)/len(dataSet))
            return out

    def cov(dataSet, mu=None, p=False):

        def vTv(v, w, factor):

            mat = array([[0. for j in range(len(w))] for i in range(len(w))])
            for i in range(len(w)):
                for j in range(len(w)):
                    mat[i][j] = factor*v[i]*w[j]
            return mat 

        def add(m, l, sign=1):
            
            if m[0] is list:
                mat = array([[0. for j in range(len(m))] for i in range(len(m))])
                for i in range(len(m[0])):
                    for j in range(len(m[0])):
                        mat[i][j] = m[i][j] + sign*l[i][j]
                return mat
                
            else:
                vec = [0. for j in range(len(m))]
                for i in range(len(m)):
                    vec[i] = m[i] + sign*l[i]
                return vec
            
        # evaluate input
        if type(dataSet[0]) is float or type(dataSet[0]) is int:
            l = len(dataSet)
            dataSet = array([dataSet])
        else:
            l = len(dataSet[0])
            
        if type(mu[0]) is not list or len(mu[0]) == l: # for vector and matrix rep. mu = [1,2]

            if type(mu[0]) is not list:
                mu = [mu, mu]
            
            print("TEST",mu)
            
            # init matrix
            matrix = [[0. for j in range(l)] for i in range(l)]
        
            # estimator
            X = dataSet
            N = len(X)
            for i in range(N):
                matrix = add(  vTv( add( X[i], mu[0], sign=-1), add( X[i], mu[1], sign=-1), 1./(1-N) ), matrix  )
            matrix = array(matrix)

            # print
            if p:
                print("covariance matrix:")
                print(matrix)

            return matrix
        
        
        else:   
            if len(mu[0]) == 1: # compare two simple sequences with mean vector mu = [float, float]

                N = len(dataSet[0])
                X = analysis.mean(dataSet[0])
                Y = analysis.mean(dataSet[1])
                XY = analysis.mean([dataSet[0][i]*dataSet[1][i]/N for i in range(N)])

                cov = XY - X*Y 

                if p:
                    print("covariance:",cov)
                return cov
   
    def var(dataSet):
        val, m = 0, analysis.mean(dataSet)
        for i in range(len(dataSet)):
            val += (dataSet[i]-m)**2
        return val

    def crudeFind(mode, fct, minRange=0, maxRange=1):

        def f(x):
            if mode == "min":
                return eval(fct)
            elif mode == "max":
                return eval("(-1)*("+fct+")")

        def crude(low, high):
            # define resolution
            r = (maxRange-minRange)/100

            # define samples of x and f(x)
            x_sample = [u(low=low, high=high) for i in range(100)]
            f_x_sample = [f(x) for x in x_sample]

            # find crude minimum position with mean of uncertainty ~2r
            x_min_crude =  x_sample[ f_x_sample.index( min(f_x_sample)) ]
            return [x_min_crude, r]

        def bisect(x_min, resolution):
            # analyze the r-radial domain to create the first section
            x_over, x_under = x_min + resolution, x_min - resolution
            
            # find the upper and lower rank that are both higher than the crude minimum
            while True:
                #print(f(x_min), f(x_over), f(x_under))
                if f(x_min) < f(x_over) and f(x_min) < f(x_under):
                    break
                else:
                    x_over += resolution
                    x_under -= resolution

                    # edge termination
                    if x_over > maxRange:
                        x_over = maxRange
                        break
                    elif x_under < minRange:
                        x_under = x_min
                        break

            return [x_under, x_over]
        
        # find min
        minimum = [minRange, f(minRange)]
        section = [minRange, maxRange]
        for i in range(3):
            new_min = crude(section[0], section[1])
            if f(new_min[0]) < minimum[1]:
                minimum[0] = new_min[0]
                minimum[1] = f(new_min[0])
                new_sec = bisect(new_min[0], new_min[1])
                section[0], section[1] = new_sec[0], new_sec[1]
        
        if mode is "max":
            return [minimum[0], -minimum[1]]
        return minimum

    def linReg(dataSet, iterations=10):

        # define a model function
        def model(phi, anchor):
            return [anchor[1] + sin(phi)*(x-anchor[0]) for x in range(len(dataSet))]
        
        # estimate phi quick
        phi_crude = np.arccos(1 / ( sqrt( 1 + (dataSet[-1]-dataSet[0])**2/len(dataSet)**2 ) ) )

        # init
        anchor = [int(len(dataSet)/2)-1, analysis.mean(dataSet)]
        chi_0 = analysis.chi_squared(dataSet, model(phi_crude, anchor))
        min = [chi_0, phi_crude]

        for i in range(iterations):
            counter = 0
            while True:
                counter += 1
                phi_prime = (1 + norm(0,1)) * min[1]
                chi = analysis.chi_squared(dataSet, model(phi_prime, anchor))
                if chi < min[0]:
                    min[0] = chi
                    min[1] = phi_prime
                    break
                elif counter == 100:
                    return [sin(min[1]), anchor[1]-0.5*sin(min[1])*len(dataSet)]
        
        return [sin(min[1]), anchor[1]-0.5*sin(min[1])*len(dataSet)]



# --- bayes --- #
class bayes:

    def parameter(data, model, N, sigma=1, x_start=0, x_end=1): # data is array([x0,y0],[x1,y1],...)
        
        # split data
        x_arr, y_arr = [data[i][0] for i in range(len(data))], [data[i][1] for i in range(len(data))]
        
        # # parse model
        # theta, i, delta = [], 0, 
        # while True
        #     if "t"+str(i) in model:
        #         theta.append(0.)
        #     i += 1

        # # random walk
        # for i in range(N):

        #     # generate candidate
        #     candidate = mGauss(theta, diag(sigma**2))

        #     # selection prbability
        #     alpha = min(1, p(candidate)/p(theta))

        #     # selection step
        #     if u() < alpha:
        #         theta = candidate
        
        # return theta

        # # functions
        # def add(m, l, sign=1):
            
        #     if m[0] is list:
        #         mat = array([[0. for j in range(len(m))] for i in range(len(m))])
        #         for i in range(len(m[0])):
        #             for j in range(len(m[0])):
        #                 mat[i][j] = m[i][j] + sign*l[i][j]
        #         return mat
                
        #     else:
        #         vec = [0. for j in range(len(m))]
        #         for i in range(len(m)):
        #             vec[i] = m[i] + sign*l[i]
        #         return vec

        # def vTv(v, w, factor):

        #     mat = array([[0. for j in range(len(w))] for i in range(len(w))])
        #     for i in range(len(w)):
        #         for j in range(len(w)):
        #             mat[i][j] = factor*v[i]*w[j]
        #     return mat 

        # def diag(eigenval, dim):
        #     mat = array([ [0. for i in range(dim)] for j in range(dim)) ])
        #     for i in range(dim):
        #         mat[i][i] = eigenval
        #     return mat

        # def det(diagonal):
        #     return diagonal[0][0]**len(diagonal[0])
        
        # def T(v_1, v_0):
        #     d = len(v_0)
        #     A = 1/( (2*pi)**(d/2) * sqrt(abs(det(diag(sigma**2, d)))) )
        #     Mv = diag(1/sigma**2, d).dot( add(v_1, v_0, sign=-1) )
        #     vTMv = vTv(add(v1, v0, sign=-1), Mv, -0.5)
        #     return dot(A, exp(vTMv))
        
        # def f(x, theta):
        #     string = model
        #     for i in range(len(theta)):
        #         string = string.replace("t"+str(i),"theta["+str(i)+"]")
        #     return eval(string)

        # def p(theta):
        #     # realization data set
        #     y = [ f(k, theta) for k in x_arr ]
        #     return exp(-analysis.xhi_squared(y_arr, y))
        


# --- stochastics --- #
class stoch:

    def process(N=100, dim=1, start=0, step=1, dist="norm(0,1)", weight=1):
        
        # test dimensions
        if type(start) == list:
            if dim == len(start):
                pass
            else:
                raise ValueError("ERROR: in process(): parameter dim must equal the dimensionality of start param e.g. dim=1 and start=float, dim=2 and start=2d-array,...")
        
        # evaluate distribution
        def phi():
            try:
                return eval(dist)
            except:
                raise NameError("ERROR: in process(): the passed dist="+str(dist)+" is not defined! Make sure this function is defined without parameters outside and pass it as a string, including the args.")
        
        # initialize start & create storage array
        out = [start]
        
        # processing
        for i in range(N):
            if dim == 1:
                out.append( float(out[-1] + weight*sqrt(step)*phi()) )
            else:
                new = []       
                for d in range(dim):
                    new.append( float(out[-1][d] + weight*sqrt(step)*phi()) )
                out.append(new)
        
        return array(out)

    def quickGauss(N=1, setSize=1):
        if setSize == 1:
            return sqrt(N)*norm(0,1)
        else:
            return [sqrt(N)*norm(0,1) for i in range(setSize)]

    def wiener(N=100, dim=1, start=0, setSize=1):
        return stoch.geomBrownian(N=N, dim=dim, start=start, setSize=setSize, diffusion=1)

    def brownian(**args):
        return stoch.wiener(**args)
    
    def geomBrownian(N=100, dim=1, start=0, step=1, diffusion=1, drift=0, setSize=1, weight=1):
        # test dimensions
        if type(start) == list:
            x = len(start)
        else:
            x = 1

        if dim == x:
            pass
        else:
            raise ValueError("ERROR: in process(): parameter dim must equal the dimensionality of start param e.g. dim=1 and start=float, dim=2 and start=2d-array,...")
        
        out = []
        for s in range(setSize):

            # initialize start & create storage array
            path = [start]
            
            # processing
            for i in range(N):

                if dim == 1:
                    path.append( float(path[-1] + drift*weight*step + diffusion*weight*sqrt(step)*norm(0,1)) )
                else:
                    new = []        
                    for d in range(dim):
                        new.append( float(path[-1][d] + drift*weight*step + diffusion*weight*sqrt(step)*norm(0,1)) )
                    path.append(new)
            out.append(path)

        if setSize==1:
            return out[0]
        else:
            return out



# --- finance --- #
class finance:

    def samuelson(price=0, T=1000, volability=1, trend=0, setSize=1):
        out = []
        for s in range(setSize):
            path = [price]
            S = price
            for i in range(T):
                S = stoch.geomBrownian(N=1, start=S, weight=S, diffusion=volability, drift=trend, setSize=1)[-1]
                path.append(S)
            out.append(path)

        if setSize == 1:
            return out[0]
        else:
            return out
    
    def gbmFit(chart):

        # init
        l = len(chart)
        s0 = chart[0]
        mu = -1.0
        
        # define model function
        def model(mu):
            return [s0*exp(mu*t) for t in range(l)]
        
        # candidate
        c = mu

        # fit the model to determine suiting trend
        chi = analysis.chi_squared(chart, model(mu))
        while mu < 1.0:
            mu += 0.001
            test = analysis.chi_squared(chart, model(mu), mode="abs")
            if test < chi:
                c = mu
                chi = test
        mu = c

        # calculate volability
        w = [(chart[i+1]-chart[i])**2 for i in range(l-1)]  # non normalized wiener process
        sigma = sqrt( sum(w)/(len(w)*(s0**2)) )
        
        return [mu, sigma]
    
    def blackScholes(C, E, riskIntRate=0.5, paths=100, diffusion=1, T=100):

        # functions
        def S(w, z):
            return C * exp( ( riskIntRate - 0.5*diffusion**2 ) * T + z * diffusion * w)

        # simulate quickly a sample of wiener-process-end-points with a simpleGauss-method
        W = stoch.quickGauss(N=T, setSize=paths)

        # estimate fair option price as function of time step
        estimate = []
        for t in range(T):
            x = 0
            for i in range(paths):
                x += exp(-riskIntRate*(T-t))
            estimate.append( (x / paths) * sum( [ ( S(W[j], 1) - S(W[j], -1) )/2 for j in range(paths)] ))

        return estimate
        

        
