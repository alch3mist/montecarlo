# Monte Carlo Methods

A library that provides a wide range of Tools made to probe, analyze and build MC-programs for computational science, physics, statistics or finance that suit modern Monte Carlo Methods. Focused on fields e.g. random generation, importance sampling, integration, Markov-processing, Bayesian estimation etc. *montecarlo* shall be seen as an add-on to other scientific state-of-the-art libraries. The vision is to obtain a little ToolBox that could "approximately" map any statistical/stochastic process or system. Vision-aimed, pushes will follow for content enrichment.

## Implementation
*montecarlo* is meant to make life as easiest as possible, and so does the implementation.
### pip3-install

    sudo pip3-install montecarlo

### .zip
Download the .zip folder and unzip it (with e.g. [winzip](https://download.winzip.com/gl/gad/winzip23.exe)). Inside the .zip folder you should find a folder /montecarlo. There are 2 ways to use this library:

 1. Run the setup (Linux/MacOS)
 `cd ./montecarlo`
 `bash setup.sh`

# Documentation

## Coverage
1. Sampling Methods
2. Analysis Tools
3. Monte Carlo Integration Methods
4. Markov Processes
5. Stochastic Processes
6. Bayesian Analysis
7. Experiments

## 1. Sampling Methods
Solid random number generation is probably the main part when setting up any Monte-Carlo-Method, and becomes an even more important task when it comes to generate un-correlated numbers. 
Most of the generators in this library build up on a seed operator ***u( )*** (in literature often called *u*) while few ones are wrapped [*numpy.random*](https://docs.scipy.org/doc/numpy/reference/routines.random.html) functions (e.g. numpy.random.norm), including some additional properties like parameter evaluation and form conventions or input-parsers which allow multi-functional use and save typing as much as possible.

## uniform sampling
## ***montecarlo.u(args, kwargs):***


>**Description:** u is the seed operator that uniformly selects values within the input boundaries. *u* comes in handy when a number unif. in `[0,1]` is needed, which is common procedure in MC algorithms. This can be performed on continuous, as well as on discrete probing spaces. All arguments are optional. If arg="d" discrete sampling is activated, otherwise continuous is default. If arg is a list or ndarray with float or int elements, an element will be picked randomly. 

**Parameter:**

 - *args*: "d"; ndarray[]; or nothing
 - *kwargs*: 
 ***low***: `int()` or `float()`; Sampling-range minimum. Must be `int()` if `arg="d"`.
 ***high***: `int()` or `float()`; Sampling-range maximum. Must be `int()` if `arg="d"`.
 ***n***: `int()`; Sample-size, if not selected returns a float(), if `n >= 1` selected an `ndarray[]*n` will be returned, yielding identical and independently ([iid](https://en.wikipedia.org/wiki/Independent_and_identically_distributed_random_variables)) generated random Variables with u().

**Return:**
- `float()` between `0` and `1`

**Examples:**
For a hypothetic [Metropolis-Hastings](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) step for instance one could write:

    #!/usr/bin/env python3
    from montecarlo import u
    
    # generate some arbitrary transition propability
    mu1, mu2 = u(), 0.8
    alpha = min(1, mu1/mu2)
    
    # selection simulation with rejection method
    if u() < alpha:
	   # accept mu1
	   mu2 = mu1
	
	print(mu2)

or a dice throw (*see also dice()*):

    result = u("d", low=1, high=6)
    #equivalently
    result = u([1,2,3,4,5,6])


## ***montecarlo.throw(args):***


>**Description:** A function build for fun to simulate a sample of many r. V. experiment. Applicable in games like Mikado, but also ideal for the Buffon's needle experiment to generate many throws at once or a parameter set for a polynom. Important: throw() builds on u() and is uniform in each parameter space, independently. Hence, the parameters are [iid](https://en.wikipedia.org/wiki/Independent_and_identically_distributed_random_variables)s.
Each random variable will then be sampled in the respective range provided. The range is either distinct,
then simply provide an array with length larger than 2.

**Parameter:**

 - ***args***: `ndarray[]; "(m * n)-matrix" e.g. array[[min1, max1], [min2, max2], [opt1, opt2,...,optm], ...]`; where `m` is either 2 then the sub-array provided acts like an interval indicating *min* and *max* for cont. sampling, or if `m>2`, distinct picking in ``[opt1, opt2,...,optm]`` will take place.

**Return:**
- `ndarray[x]*n`; with n being the number of provided intervals "=" number of parameters; with x being `float(), int() or str()`

**Examples:**
Ever wanted to generate a random date? see [birthday problem](https://en.wikipedia.org/wiki/Birthday_problem) as one could simulate it and prove it numerically...

    import montecarlo as mc
    
    months = ["Jan", "Feb", "March", "April", "May"]
    days = [i+1 for i in range(31)]
    # gen. random date
    while True:
	    date = mc.throw(months, days)
	    if "Feb" in date[0]:
		    if date[1] <= 27:
			    break 	
	    
	    elif "April" in date[0]:
		    if date[1] <=29:
			    break
	    else:
		    break
	print(date)

One would like to randomly generate a crude MC-parameter set with the aim of fitting a polynom (more efficient with bayes or stoch):

	import montecarlo as mc
	import matplotlib.pyplot as plt

	# take some random data
	data = mc.u(low=6, high=9, n=10)

	# model-function
	def  f(x, p1, p2, p3):
		return p1 * (x**2) + p2 * x + p3
		
	# least square method
	min, p_opt =  100, None
	for i in  range(10000):
		# generate some random parameters
		p = mc.throw([-0.1,0.1], [-0.5,1], [4,9])

		# model set
		model = [f(i, p[0], p[1], p[2]) for i in  range(10)]

		if mc.analysis.xhi_squared(data, model) <  min:
			min, p_opt = mc.analysis.xhi_squared(data, model), p

	# optimal parameters
	print(p_opt)

	plt.plot(data)
	plt.plot([f(i, p_opt[0], p_opt[1], p_opt[2]) for i in  range(10)])
	plt.show()
		

## non-uniform
## ***montecarlo.norm(mean, var, n=1):***


>**Description:** Identical to [numpy.random.normal](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.normal.html#numpy.random.normal). Normal variables generator in 1-d with mean *mean*  and variance *var*. 

**Parameter:**

 - ***mean*:** `float()`
 - ***var*:** `float()` greater than `0` by definition
 - ***n*:** `int()` set size; if `n>1` returns `ndarray[]` else `float()`

**Return:**
- `ndarray[]*n`; 

**Examples:**
fill a room with hundred people:

   
    # sample approprietly (according to todays mankind) IQ-normal-dist. as an approx. of poisson
    IQs = mc.norm(100, 15*15, 100) # 1 sigma is 15 iq-points

	
simulate if a [John von Neumann](https://en.wikipedia.org/wiki/John_von_Neumann) is among them:

    if max(IQs) >= 160:
	    print(True)
	else:
	    print(False)
	    
output:

	[ 98.01614732  98.23376763 108.88588334  95.82596154  88.55111025
	 109.37893363  78.85527801 108.01217873 106.83022744 103.81149372
	 114.8735221  103.48307787 129.43993753  80.0963636   89.31819258
	  89.38329145  86.22692018 132.54971123  89.82499962  89.51259058
	  99.80708447 100.22311886 118.41302029 102.07179608 111.6268568
	 126.98961593  91.17645607 129.78034419 107.29215772  98.22213389
	 105.4515667   98.26412812  96.11298661 115.32130087  98.17652571
	  78.94254085  87.33011518  87.02102152 120.46154951  98.53850548
	 107.69933731  78.8379323  112.94437507  78.63822705  82.26338951
	 125.88767676 113.43897512 106.24736764 119.15842889  77.9327918
	  86.16594545 111.74196997 114.7904486   77.30925416 102.219494
	 122.33243757 104.8974364   91.95321882 103.65432122 106.86142119
	 106.39644483 111.71384866 115.3261556   97.33273423 108.63454774
	  94.20211506  97.20643656  95.0367414  122.25160362  69.54949703
	  97.8302212   63.99834614  90.26623855 101.30932488  73.05508431
	  86.46301941  96.86567082  89.11793309 132.55632825  65.14136726
	  66.83752453  97.28110851  88.40041569  85.37427134  92.2795718
	  61.04871152  89.72479374 100.5718652   89.73135315 117.17077048
	  79.02775753 109.55652655 114.02535723 105.23437504 118.02654952
	  84.35486855  78.04960868 102.91425836  95.56816181 121.12125008]
	False

  it will take an "some" time to find John and hence for this statement to turn True...


## ***montecarlo.boxMuller(n=1):***
>**Description:** Seed-u()-based normal-dist. vector generator. `2` iid `u1, u2`  are sampled uniformly in `[0,1]`. Then `y1 = sin(2*pi*u2)*sqrt(-2ln(u1))` and `y2 = cos(2*pi*u2)*sqrt(-2ln(u1))` are iid generated from `~N(0,1)` (standard-gaussian). Box-Muller creates 2-dimensional `N(0,1)`-dist. numbers only. See [publication](https://projecteuclid.org/euclid.aoms/1177706645) (1958).

**Parameter:**

 - ***n*:** `int()` set size; if `n>1` returns `ndarray[]` else `float()`

**Return:**
- `ndarray[]*2` single vector if `n=1` (default); else `ndarray[]*n`  set of normal `N(0,1)` vectors

**Examples:**
Create a std.-gaussian clouds:

	import montecarlo as mc
	import matplotlib.pyplot as plt
	
	N =  100000
	data = mc.boxMuller(N)
	# loads
	x_axis = []
	y_axis = []

	for i in  range(N):
		x_axis.append(data[i][0])
		y_axis.append(data[i][1])

	# plot
	plt.text(-4, -4, "Box-Muller generated Normal distribution\nN="+str(N))
	plt.plot(x_axis,y_axis,"k,")
	plt.show()
output:
[play around](https://upload.wikimedia.org/wikipedia/commons/1/1f/Box-Muller_transform_visualisation.svg)
  
## ***montecarlo.mGauss(mean, sigma, n=1):***
>**Description:** Seed-u()-based multivariate-normal-dist. vector generator in arbitrary dimensions. With sigma, the covariance matrix of choice it is possible by linear transformation (and therefore less iterations) to obtain `~N(mean, sigma)` dist. vectors with `len(mean)` dimensions. This is achieved by inversion sampling using [cholesky factorization](https://en.wikipedia.org/wiki/Cholesky_decomposition).

**Parameter:**

 - ***sigma*:** `ndarray[]; "(n * n)-matrix"` with n being the dimension of *sigma*
 - ***n*:** `int()` set size; if `n>1` returns `ndarray[]` else `float()`

**Return:**
- `ndarray[]*2` single vector if `n=1` (default); else `ndarray[]*n`  set of normal `N(0,1)` vectors

**Examples:**

	# define a covariance matrix sigma:
	sigma = array([ [0.983, 0.0175, 0.0014],
			[0.0175, 0.961, 0.021],
			[0.0014, 0.021, 0.976] ])
			
	# and generate a set of sigma-normal-distributed (multi-dim gaussian) vectors with mean 000:
	print("generate set of random normal dist. vectors with covariance matrix:\n",sigma, "\n")
	Set = mc.mGauss(array([0,0,0]), sigma, n=10000)
	print(Set)



Use *montecarlo.reject* to sample from any function provided as string object:
	 
	import montecarlo as mc
	import matplotlib.pyplot as plt
	import numpy as np

	# sample from f_x using rejection method
	f_x =  "(x-np.log(2))**2+2*x**5"
	sample = mc.reject(f_x, minRange=-1, maxRange=1, n=1000)

	# plot
	plt.hist(sample, bins=20)
	plt.show()

## ***montecarlo.reject(f, g=None, G_inv=None, minRange=0, maxRange=1, n=1):***

>**Description:** The [rejection method](https://en.wikipedia.org/wiki/Rejection_sampling) by [John von Neumann](https://de.wikipedia.org/wiki/John_von_Neumann) samples with a favored weight described by function `f(x)` (similar to importance sampling) but with knowing a "simpler" function *g* that is everywhere greater than *f*. The oldest montecarlo-experiment one can think of and which based on rejection-sampling was probably [buffon's needle problem](https://en.wikipedia.org/wiki/Buffon%27s_needle_problem) which Buffon used to approximate `π` by a large random-throw-sample - according to the [weak law of large numbers](https://en.wikipedia.org/wiki/Law_of_large_numbers#Weak_law) a large set of numbers lead this estimator to converge (unbiased) towards `π`. For further details see [Knuth: The art of computer programming](https://dl.acm.org/citation.cfm?id=260999).
If f is not known as only data is given, then create a regression of the data, using e.g. *montecarlo.bayes.param*, and provide a function string to *f* including the best-fit-parameters. For the functions `f(x) < g(x)` for all `x` within `[minRange, maxRange]` sample from `g(x)` by inversion first using *G_inv* the inverse [probability distribution](https://en.wikipedia.org/wiki/Probability_distribution#Cumulative_distribution_function) of *g*. If *G_inv* is not provided (independent of the choice of *g* both functions will be build as `g(x) = max{ f(x) }` and `G_inv(x) = 1/g * x` with the help of *montecarlo.analysis.crudeFind("max")*. Then if `alpha = u()` is a generated number in `[0,1]` uniformly then `y = G_inv( alpha )` is distributed acc. to *g* if `y` is accepted with probability `f(y)/g(y)`. If only *f* is known with the task to sample randomly ( ~*f*-distributed ) in `[minRange, maxRange]`, simply set parameters *f, minRange, maxRange* - this works fine as *g* and *G_inv* are optional parameters.

  

**Parameter:**


-  ***f*:**  `str()` function string (e.g. f="x**2"); variable must be declared as "x"
-  ***g*:**  `str()` or `None` optional function string
-  ***G_inv*:**  `str()` or `None` optional function string
-  ***minRange*:**  `float()` or `int()` the lower sampling bound
-  ***maxRange*:**  `float()` or `int()` the upper sampling bound
-  ***n*:**  *int()* set size, default 1


**Return:**
-  `float()` if **n**=1 (default); else `ndarray[]*n`; number or set of numbers iid dist. acc. to *f* in defined range

  

**Examples:**

Use *montecarlo.reject* to sample from any function provided as string object:

	import montecarlo as mc
	import matplotlib.pyplot as plt
	import numpy as np
	 
	# sample from f_x using rejection method
	f_x = "(x-np.log(2))**2+2*x**5"
	sample = mc.reject(f_x, minRange=-1, maxRange=1, n=1000)

	# plot
	plt.hist(sample, bins=20)
	plt.show()

## 2. Integration

## ***montecarlo.integration***

MC integration package provides some methods for quick integration of functions. The variance of the estimators is outperformed by numerical methods (under 8 dimensions, from here MC is faster), but montecarlo provides a variance reduction method, aswell and still the MC-estimators are fast. 

## ***montecarlo.integration.crude(fct, minRange=0, maxRange=1, N=100, setSize=1):***
>**Description:** *crude()* is a crude estimator which randomly samples f(x) and symply takes the mean. This naive approach (acc. to the law of large numbers) converges to the integral of *fct*  evaluated in `[minRange, maxRange]`. For better approaches see below. 
>Provide the function of interest as string e.g. fct = "x**2". The other parameters are optional but keep in mind that N should be adjusted to fit the defined range. See also [MC integration](https://en.wikipedia.org./wiki/Monte_Carlo_integration) 

**Parameter:**

 - ***fct*:** `str()` function string (e.g. f="x**2"); variable must be declared as "x"
 - ***minRange*:** `float()` or `int()` the lower sampling bound
 - ***maxRange*:** `float()` or `int()` the upper sampling bound
 - ***N*:** `int()` random sample size for x, the greater N is the more the algorithm will be slowed down, but the estimate will be better.
 - ***setSize*:** `int()` set size, default 1

**Return:**
- `float()`  if **setSize**=1 (default); else `ndarray[]*n`; number or set of numbers iid dist. acc. to *f* in defined range

**Examples:** 

    import montecarlo as mc
    import matplotlib.pyplot as plt
    import numpy as np
    
    # create a function string e.g. gauss function
    f_x =  "np.exp(-x**2)"
    
    # integrate
    I = mc.integration.crude(f_x, minRange=-100, maxRange=100, N=10**6)
    
    print("I²:", I**2)
    print("deviation from numpy pi:", abs(np.pi-I**2)/np.pi*100, "%")


## ***montecarlo.integration.stratified(fct, minRange=0, maxRange=1, bins=10, N_per_bin=10, setSize=1):***
>**Description:** a. s. crude sampling leads to high variance of the estimation, but not if it is seen as a standard procedure and performed iteratively implemented in a wider method. The nice side effects of [stratified sampling](https://en.wikipedia.org/wiki/Stratified_sampling) compared to e.g. crude sampling is lowering the number of needed sample points for a "sufficient result" and suppressing the variance. For this method the integration range is partitioned automatically and a mean estimation is performed by our crude procedure. Then the means get summed up. The number of "strata" or *bins* and the number of sample points per bin *"N_per_bin"* should be provided to increase the accuracy, for a time fee obviously. 

**Parameter:**

 - ***fct*:** `str()` function string (e.g. f="x**2"); variable must be declared as "x"
 - ***minRange*:** `float()` or `int()` the lower sampling bound
 - ***maxRange*:** `float()` or `int()` the upper sampling bound
 - ***bins*:** `int()` number of bins or partions crude will be performed
 - ***N_per_bon*:** `int()` random sample size for x, the greater *N_per_bin* is the more the algorithm will be slowed down, but the estimate will be better.
 - ***setSize*:** `int()` set size, default 1

**Return:**
- `float()`  if **setSize**=1 (default); else `ndarray[]*n`; number or set of numbers iid dist. acc. to *f* in defined range

**Examples:** 

	import montecarlo as mc
	import matplotlib.pyplot as plt
	import numpy as np

	# create a function string e.g. gauss function
	f_x =  "np.exp(-x**2)"

	# integrate
	I = mc.integration.stratified(f_x, minRange=-100, maxRange=100, N_per_bin=100, bins=1000)

	print("I²:", I**2)
	print("deviation from numpy pi:", abs(np.pi-I**2)/np.pi*100, "%")

## variance reduction

## ***montecarlo.integration.antithetic(fct, minRange=0, maxRange=1, N=100, setSize  =  1):***
>**Description:** [Antithetic variaties](https://en.wikipedia.org/wiki/Antithetic_variates) is a method by explotation of the variance [definition](https://en.wikipedia.org/wiki/Variance#Basic_properties) `Var[I+I'] = Var[I] + Var[I'] + cov[I,I']` . If one is able to find a function `f(x)` with unbiased estimator *I* which is monotonous (simply speaking) in a certain range , then one could infer another unbiased estimator *I'* of `f(1-x)` in the same range. As both estimators (which are obviously correlated by construction) converge to the same value as `E[1/2*(I+I')] = E[I] = E[I']` and the derivatives have opposite sign `f'(x) ~ -f'(1-x)` one can show that the covariance `cov[I,I'] < 0` which leads to a negative increment in the variance `Var[1/2(I+I')] = 1/4 * (Var[I] + Var[I'] + 2*cov[I,I'])`. Make sure the provided *fct* string fulfills the conditions above.

**Parameter:**

 - ***fct*:** `str()` function string (e.g. f="x**2"); variable must be declared as "x"
 - ***minRange*:** `float()` or `int()` the lower sampling bound
 - ***maxRange*:** `float()` or `int()` the upper sampling bound
 - ***N*:** `int()` random sample size for x, the greater *N* is the more the algorithm will be slowed down, but the estimate will be better.
 - ***setSize*:** `int()` set size, default 1

**Return:**
- `float()`  if **setSize**=1 (default); else `ndarray[]*n`; number or set of numbers iid dist. acc. to *f* in defined range

**Examples:** 
Here is an example of variance reduction of one provided function, run it by yourself and prove empirically that the variance drops with a factor of ~ 20-40:

	#!/usr/bin/env python3
	import montecarlo as mc

	# VARIANCE REDUCTION

	# lets integrate the function 1/(1+x) from 0 to 1

	# using two methods and compare the corresponding variances:
	f =  "1./(1+x)"  # create a function string, it is mandatory to call the integration-variable "x"

	# for each method create a set of estimations of f (N=100 is default)
	I_crude = mc.integration.crude(f, setSize=100)
	I_anti = mc.integration.antithetic(f, setSize=100) # reduced variance candidate

	# analize crude
	I_crude_mean = mc.analysis.mean(I_crude)
	I_crude_var = mc.analysis.var(I_crude)

	# analize antithetic
	I_anti_mean = mc.analysis.mean(I_anti)
	I_anti_var = mc.analysis.var(I_anti)

	# analize covariance - check for negativity
	mc.analysis.cov([I_crude, I_anti], mu=[[I_crude_mean], [I_anti_mean]], p=True)

	# compare them
	print(" ", "crude", "\t","antith")
	print("mean", I_crude_mean, I_anti_mean)
	print("var ", I_crude_var, I_anti_var)

output:

	covariance: -0.4748310399192177
	     crude 	 antith
	mean 0.6921606413616459 0.6929422143005496
	var  0.020204526482090143 0.0005168793598896863

## 3. Markov Processes

## ***montecarlo.markov.ising(kwargs):***
>**Description:** The classical simulation of spontaneous magnetization in solids using the [Metropolis-Hastings](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) method executed on the [ising model](https://en.wikipedia.org/wiki/Ising_model). The system will reach equilibrium at the critical temperature *T*=2.269, if J_v, J_h = 1.0 the vertical and horizontal coupling constants, respectively - these can be chosen separately. Note that it is practical to provide the parameters either as json or dict object (see example below).

**Parameter:**

 - ***fct*:** `str()` function string (e.g. f="x**2"); variable must be declared as "x"
 - ***minRange*:** `float()` or `int()` the lower sampling bound
 - ***maxRange*:** `float()` or `int()` the upper sampling bound
 - ***N*:** `int()` random sample size for x, the greater *N* is the more the algorithm will be slowed down, but the estimate will be better.
 - ***setSize*:** `int()` set size, default 1

**Return:**
- `float()`  if **setSize**=1 (default); else `ndarray[]*n`; number or set of numbers iid dist. acc. to *f* in defined range

**Examples:**
With the sampling parameter set to 1000 we will store every 1000th markov state i.e for ising it is a panel or matrix of spin states in a space (also provided) [-1, 1]. The size of the panel is *length* ², so with *length* = 100 we will create 10000 sites.

	#!/usr/bin/env python3

	import montecarlo as MC, matplotlib.pyplot as plt
	
	suite = {
		"sampling": 1000,
		"steps": 1000000,
		"space": [-1, 1],
		"T": 2.3,
		"length": 100,
		"j_v": 1.,
		"j_h": 1.,
		"stdout": True,
		"start": "cold"
	}

	  

	# simulate
	storrage = MC.markov.ising(**suite)

	# log the mean magnetization per site over time
	mag = []
	print("\nlog magnetization...", end="\r")
	file  =  open("magnetization.log", "w+")
	for x in storrage:
		N, sum  =  len(x[0]), 0
		for i in  range(N):
			for j in  range(N):
				sum  += x[i][j]
				mag.append(float(sum)/N**2)
				file.write(str(float(sum)/N**2))
	file.close()

	# plot
	plt.plot(mag)
	plt.show()
	print("creating plots...\n", end="\r")
	for i in  range(len(storrage)):
		print(str(i), end="\r")
		plt.imshow(storrage[i])
		plt.savefig("./plots/"+str(i)+".png")

## Analysis
*montecarlo.analysis* provides some useful analysis methods, most of them were build in first place to analyze the method-outputs from other *montecarlo* packages, but surely allow universal usage as well.

## *montecarlo.analysis.chi_squared(data_array, model_array, order=2):*
>**Description:** [χ2](https://en.wikipedia.org/wiki/Chi-squared_test) (described by [Pearson](https://en.wikipedia.org/wiki/Pearson%27s_chi-squared_test)) distribution-hypothesis test. In order to see if there is "correlation" or equality (Hypothesis) between two data-array-distributions *d1* and *d2* one could calculate χ2(*d1*, *d2*) where d2 is conventionally considered as the proposal distribution, then χ2 stands for the squared-deviation of sampled data d1 from proposed (hypothesis-) distribution d2. In practice the permutation of *data_array* and *model_array* leads to different output. **Note:** The arrays can either present distributions (e.g. 1000-bits-snippet would have an array of length 2 as dist. with ~ [500, 500]), or evaluated function points (e.g. `[f(x1), f(x2),...]` ) but only if the elements of *model_array* fulfill `f_model(xi)≠ 0 ∀ i`, then this is equivalent to  a functional-distribution *f* and comes in handy when doing regression-fitting for instance. The parameter *order* was kept variable for other moments (if needed). Validate the result within a [significance level](https://en.wikipedia.org/wiki/Statistical_significance), if χ2 is smaller than the critical value the hypothesis is accepted otherwise it will be rejected as False.

**Parameter:**

 - ***data_array*:** `list()`or `array()` sampled data-array
 - ***model_array*:**  `list()`or `array()` expected data-array that suits the hypothesis (e. g. uniform)
 - ***order*:** *optional* `int()` [moment](https://en.wikipedia.org/wiki/Moment_%28mathematics%29)-order


**Return:**
- `float()`  

**Examples:**
Let's say we have two bit-strings and we want to know if they were correlated in their distribution, assuming that both were generated from a uniform distribution function (hypothesis):

	import montecarlo as mc
	import matplotlib.pyplot as plt
	import numpy as np

	# bit strings
	b1 = mc.u([0,1], n=1000)
	b2 = mc.u([0,1], n=1000)

	# distributions
	d1 = [b1.count(0), b1.count(1)]
	d2 = [b2.count(0), b2.count(1)]

	# test dist correlations
	print(mc.analysis.chi_squared(d1, d2))

	# plot
	plt.plot(d1)
	plt.plot(d2)
	plt.show()

the bit strings can only be analyzed as distribution representation as if interpreted as function solutions, 0 would lead to division error. For a regression application see example of *montecarlo.throw()*.

## *montecarlo.analysis.indep(data1, data2, bins=None):*
>**Description:** [χ2](https://en.wikipedia.org/wiki/Chi-squared_test) independence-hypothesis test allows independence tests of two samples *data1* and *data2*. Each "correlation" (dependence) between each element of *data1* with *data2* are taken into account, achieved by relative occurrences. Properties may also be `str()` i.e. `data1 = [str(), float()]` which allows statistical hypothesis tests for e.g. surveys. The needed distributions of the data arrays will be build automatically which contain the empirical occurrences. Make sure to provide the bins parameter if the data array include `float()` objects (see example) or is not equidistantly distributed i.e. the number of bins is then the number of property-sections.

**Parameter:**

 - ***data1*:** `list()`or `array()` occurences
 - ***data2*:**  `list()`or `array()` expected data-array that suits the hypothesis (e. g. uniform)
 - ***order*:** *optional* `int()` [moment](https://en.wikipedia.org/wiki/Moment_%28mathematics%29)-order


**Return:**
- `float()`  

**Examples:**


## *montecarlo.analysis.mean(dataSet):*
>**Description:** Mean of `float()` list returns `float()` **or** mean distribution of a set of `float()` lists returns list with floats.

**Parameter:**

 - ***dataSet*:** `list()` e.g. `[float(), float(),...]` or `array(list(), list(),...)` e.g. `[list_1, list_2,..., list_N]`. Becomes useful when fitting distributions or chart data.


**Return:**
- `float()` or `N`-dimensional `list()` with `[mean(list_1), mean(list_2), ..., mean(list_N)]`

**Examples:**

Simple mean calculations with `float()` list:

	arr = [1,2,3,4,5]
	print(mc.analysis.mean(arr))
should return `3.0`.

imagine we have given a set array of many possible paths of a wiener process, which are lists or arrays again and we want to calculate the mean path, this can be realized in two lines:

	Set = mc.stoch.geomBrownian(setSize=5, drift=0.05, diffusion=0.1)
	mean = mc.analysis.mean(Set)

we can plot the sets and the mean where the red path is the mean:

	for s in Set:
		plt.plot(s, "b")
	plt.plot(mean, "r")
	plt.show()

## *montecarlo.analysis.cov(dataSet, mu=None, p=False):*
>**Description:** For [covariance](https://en.wikipedia.org/wiki/Covariance) tests on mutual data-set-dependencies - really useful to guess a distribution from a data-set or to seek for correlations in data e.g. bit strings, test for independence and look for similarities between two arrays only.  This is a multi-functional method again and can be used mainly in two ways. For example for a set of multivariate Gauss vectors (arrays with e.g. mGauss() ) surely generated from a given covariance matrix `C[i][j]`, but which is unknown, the procedure is to insert a set of such arrays as the dataSet, then cov() will estimate that matrix C  from the given data. The magnitude of i-th and j-th element in the covariance-matrix indicates the stochastic dependence between the i-th and j-th position of an hypothetical sampling distribution, estimated from traced mutual abnormality-account-measures, realized by mean deviations. i.e. it presents the (not-normalized) mean correlation between i-th element and j-th element obtained from all occurrences accounted from the trace out of the data set. If the parameter *mu* is provided, as the expected mean of the data set, the deviation will be obtained from the provided mean-vector *mu*. The dimension of the mean has to suit the dimension of the vectors/arrays/lists in the data set. The second way allows to measure the empirical covariance between two lists. For this provide a single vector/array for *dataSet* and the second (no matter which for) as *mu*. If *mu* is a scalar float() an mandatorily the data set contains float then this is identical to a *mean()* function of an array. In order to make output of the matrix easier you can simply set the *p*-flag to Boolean True.

**Parameter:**

 - ***dataSet*:** `list()` e.g. `[float(), float(),...]` or `array(list(), list(),...)` e.g. `[list_1, list_2,..., list_N]`
 - ***mu*:**  `list()` with `float()` entries or `list()` with  `[[float()],[float()],...]` the second is for respective distribution indication.


**Return:**
- ndarray([]) covariance matrix; or `float()` mean covariance between two 

**Examples:**

  Estimate the covariance matrix of a given set of vectors from an unknown distribution:
  
	#!/usr/bin/env python3
	import montecarlo as mc
	from numpy import array, matrix

	# Reconstruct the covariance of MC-generated gaussian-vectors

	# define a covariance matrix sigma:
	sigma = array([ [0.983, 0.0175, 0.0014],
			[0.0175, 0.961, 0.021],
			[0.0014, 0.021, 0.976] ])

	# and generate a set of sigma-normal-distributed (multi-dim gaussian) vectors with mean 000:
	print("generate set of random normal dist. vectors with covariance matrix:\n",sigma, "\n")
	Set = mc.mGauss(array([0,0,0]), sigma, n=10000)

	# calculate the mean distribution in each dimension:
	M = mc.analysis.mean(Set)	  

	# reconstruct covariance matrix empirically from the data:
	print("estimate covariance matrix from the data, assuming the mean was:", M)
	mc.analysis.cov(Set, mu=M, p=True)
	
as mentioned in the description, you can use it differently e.g. to check the scalar covariance of two differently sampled vectors/arrays:
	
	
	mc.analysis.cov([set_of_data_arrays_1, set_of_data_arrays_2], mu=[[mean_1], [mean2]], p=True)

in order to indicate that the mean is not a vector but a list of numbers where each number refers to the corresponding index-value in the data_array (instead as before where mu was a mean array which is equally applied to each list/vector/array in the dataSet) use additional brackets as demonstrated above.
Below you can see exactly this application when it comes to covariance-test for antithetic variaties (also see *antithetic()*):

	#!/usr/bin/env python3
	import montecarlo as mc
	
	# VARIANCE REDUCTION  

	# lets integrate the function 1/(1+x) from 0 to 1
	# using two methods and compare the corresponding variances:


	f =  "1./(1+x)"  # create a function string, it is mandatory to call the integration-variable "x"
	  

	# for each method create a set of estimations of f (N=100 is default)
	I_crude = mc.integration.crude(f, setSize=100)
	I_anti = mc.integration.antithetic(f, setSize=100) # reduced variance candidate

	  
	# analize crude
	I_crude_mean = mc.analysis.mean(I_crude)
	I_crude_var = mc.analysis.var(I_crude)


	# analize antithetic
	I_anti_mean = mc.analysis.mean(I_anti)
	I_anti_var = mc.analysis.var(I_anti)
	

	# analize covariance - check for negativity
	mc.analysis.cov([I_crude, I_anti], mu=[[I_crude_mean], [I_anti_mean]], p=True)


	# compare the covariances of both methods
	print(" ", "crude", "\t","antith")
	print("mean", I_crude_mean, I_anti_mean)
	print("var ", I_crude_var, I_anti_var)
