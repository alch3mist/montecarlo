#!/usr/bin/python
# -*- coding: utf-8 -*-
import montecarlo as mc
from numpy import sqrt
import matplotlib.pyplot as plt

class deck:
    n = 52
    s = [i for i in range(1,n+1)]   # deck ordered
    r = mc.shuffle(s)               # shuffled deck

def transition(array):  # draws random card and place it on top
    arr = array.copy()
    pick = mc.u(array)
    arr.remove(pick)
    arr.insert(0,pick)
    return arr

def vectorDiffMag(vector1, vector2):
    out = []
    if len(vector1) == len(vector2):
        for i in range(len(vector1)):
            out.append( (vector1[i]-vector2[i])**2 )
        return sqrt(sum(out))
    else:
        raise ValueError("ERROR: vectors have different length.")

deltas = []
for i in range(1000):
    deck.s, deck.r = transition(deck.s), transition(deck.r)
    delta = vectorDiffMag(deck.s, deck.r)
    deltas.append(delta)

# plot
plt.plot(deltas)
plt.show()
