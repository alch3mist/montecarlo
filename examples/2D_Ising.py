#!/usr/bin/env python3
import montecarlo as MC, matplotlib.pyplot as plt

suite = {
    "sampling": 1000,
	"steps": 1000000,
	"space": [-1, 1],
	"T": 2.3,
	"length": 100,
    "j_v": 1.,
    "j_h": 1.,
	"stdout": True,
	"start": "cold"
}

# simulate
storrage = MC.markov.ising(**suite)

# log the mean magnetization per site over time
mag = []
print("\nlog magnetization...", end="\r")
file = open("magnetization.log", "w+")
for x in storrage:
    N, sum = len(x[0]), 0
    for i in range(N):
        for j in range(N):
            sum += x[i][j]
    mag.append(float(sum)/N**2)
    file.write(str(float(sum)/N**2))
file.close()


# plot
plt.plot(mag)
plt.show()
print("creating plots...\n", end="\r")
for i in range(len(storrage)):
    print(str(i), end="\r")
    plt.imshow(storrage[i])
    plt.savefig("./plots/"+str(i)+".png")


