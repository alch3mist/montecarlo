import montecarlo as mc
import matplotlib.pyplot as plt
import numpy as np

# physical properties
u = 1.66 * 10**(-27)
m = 14*u    # nitrogen
T = 295
kB = 1.38064852 * 10**(-23)

# define maxwell boltzmann distribution for a state x
def H(x):
    E = 0
    for i in range(len(x)):
        z = m* i**2* 1./(2*kB*T)
        E += i**2 * np.exp(z)
    return E

start = [100]*1000
start[100] = 999900

suite = {
    "sampling": 1000,
	"steps": 10000,
    "conservative": True,
	"stdout": True,
	"start": start
}


# simulate thermalization
Set = mc.markov.chain(H,**suite)

# plot
plt.plot(Set[-1])
plt.show()