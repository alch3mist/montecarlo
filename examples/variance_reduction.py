#!/usr/bin/env python3
import montecarlo as mc 

# VARIANCE REDUCTION

# lets integrate the function 1/(1+x) from 0 to 1
# using two methods and compare the corresponding variances:


f = "1./(1+x)"    # create a function string, it is mandatory to call the integration-variable "x"


# for each method create a set of estimations of f (N=100 is default)
I_crude = mc.integration.crude(f, setSize=100)
I_anti = mc.integration.antithetic(f, setSize=100) # reduced variance candidate

# analize crude
I_crude_mean = mc.analysis.mean(I_crude)
I_crude_var = mc.analysis.var(I_crude)

# analize antithetic 
I_anti_mean = mc.analysis.mean(I_anti)
I_anti_var = mc.analysis.var(I_anti)

# analize covariance - check for negativity
mc.analysis.cov([I_crude, I_anti], mu=[[I_crude_mean], [I_anti_mean]], p=True)

# compare them
print("    ", "crude", "\t","antith")
print("mean", I_crude_mean, I_anti_mean)
print("var ", I_crude_var, I_anti_var)




