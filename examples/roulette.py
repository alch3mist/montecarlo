import montecarlo as mc 
import os
from time import sleep

def cprint(text, col):
    if text == None:
        text = "00"
    else:
        text = str(text)
    if len(text) == 1:
        text = " " + text
    if col == "green":
        return '\033[0;37;42m' + text + '\033[0m'
    if col == "red":
        return '\033[0;37;41m' + text + '\033[0m'
    if col == "black":
        return '\033[0;37;48m' + text + '\033[0m'

def table(position):
    class num(object):
    
        def __init__(self, colour, digit, position):
            """ Create a new point at the origin """
            self.colour = colour
            self.digit = digit
            self.position = position

    position = position % 37

    width = os.get_terminal_size().columns
    if width > 38:
        width = 38
    arr = [
        num("green", 0, 0),
        num("black", 28, 1),
        num("red", 9, 2),
        num("black", 26, 3),
        num("red", 30, 4),
        num("black", 11, 5),
        num("red", 7, 6),
        num("black", 20, 7),
        num("red", 32, 8),
        num("black", 17, 9),
        num("red", 5, 10),
        num("black", 22, 11),
        num("red", 34, 12),
        num("black", 15, 13),
        num("red", 3, 14),
        num("black", 24, 15),
        num("red", 36, 16),
        num("black", 13, 17),
        num("red", 1, 18),
        num("green", None, 19),
        num("red", 27, 20),
        num("black", 10, 21),
        num("red", 25, 22),
        num("black", 29, 23),
        num("red", 12, 24),
        num("black", 8, 25),
        num("red", 19, 26),
        num("black", 31, 27),
        num("red", 18, 28),
        num("black", 6, 29),
        num("red", 21, 30),
        num("black", 33, 31),
        num("red", 16, 32),
        num("black", 4, 33),
        num("red", 23, 34),
        num("black", 35, 35),
        num("red", 14, 36),
        num("black", 2, 37),
    ]
    if width % 2 == 0:
        a, b = int(position-width/2), int(position+width/2)
    else:
        a, b = int(position-width/2), int(position+width/2)
    if b > 37:
        b = b%37
        a = a - 37

    string = ""
    for i in range(a, b):
        #print(a,b)
        string += cprint(arr[i].digit, arr[i].colour)
    return string

def roll():
    # entry
    entryPoint = mc.u("d", low=0, high=37)
    tipOff = mc.u("d", low=0, high=64)
    slowdown = mc.u("d", low = 0, high = 4)
    # roll two rounds
    for i in range(63):
        print("                                 " + "°" + "                                ", end = "\r\n")
        print(table(i), end="\r")
        sleep(0.2)
        os.system("clear")
    
    for i in range(tipOff):
        print("                                 " + "°" + "                                ", end = "\r\n")
        print(table(i), end="\r")
        if tipOff - i < slowdown:
            sleep(1.0)
        else:
            sleep(0.5)
        if i < tipOff-1:
            os.system("clear")
    print("\n")

while True:
    roll()
    if input("Retry? (y/n)") == "y":
        os.system("clear")
    elif "n":
        break