#!/usr/bin/env python3

import montecarlo as mc 
from numpy import sin, exp

# read in data
f = open("data.txt", "r")
data = [float(i) for i in f.read().split("\n")]
f.close()

# create model fit function
def f(x, theta1, theta2):
    return theta1*x + sin(theta2*x)

# define gaussian likelihood
def p(data, theta1, theta2):
    model_data = [f(i, theta1, theta2) for i in range(len(data))]
    return exp(-mc.analysis.xhi_squared(data, model_data))

