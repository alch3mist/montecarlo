#!/usr/bin/env python3
import montecarlo as mc
from numpy import array, matrix

# Reconstruct the covariance of MC-generated gaussian-vectors

# define a covariance matrix sigma:
sigma = array([ [0.983, 0.0175, 0.0014], 
                [0.0175, 0.961, 0.021], 
                [0.0014, 0.021, 0.976] ])


# and generate a set of sigma-normal-distributed (multi-dim gaussian) vectors with mean 000:
print("generate set of random normal dist. vectors with covariance matrix:\n",sigma, "\n")
Set = mc.mGauss(array([0,0,0]), sigma, n=10000)

# calculate the mean distribution in each dimension:
M = mc.analysis.mean(Set)

# reconstruct covariance matrix empirically from the data:
print("estimate covariance matrix from the data, assuming the mean was:", M)
mc.analysis.cov(Set, mu=M, p=True)