#!/usr/bin/env python3
import montecarlo as mc 
import matplotlib.pyplot as plt

def f():
    return mc.norm(0, 1)

# generate brownian motion
motion_1d = mc.experiment.brownian(N=10000)
motion_2d = mc.experiment.brownian(N=10000, dim=2, start=[0,0])

# plot

# 1d
plt.plot(motion_1d)
plt.show()

# 2d
x, y = [], []
for i in range(len(motion_2d)):
    x.append(motion_2d[i][0])
    y.append(motion_2d[i][1])
plt.plot(x, y)
plt.show()

